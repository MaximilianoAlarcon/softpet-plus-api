using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using System.Configuration;
using System.Runtime.Serialization;
using AWSServerless1.Modelos;
using System.Globalization;
using System.Reflection;


/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
*/

namespace AWSServerless1.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : ControllerBase
    {


        private string ConexionString = "Server=CLOUD.CLEARSKY.COM.AR,15016;Database=CSKY-PET-VETYA;User Id=usr-csky-pet-vetya-web;Password=CzNsjxXfKDe5SVWz;TrustServerCertificate=True;";

        private String sqlDatoToJson(SqlDataReader dataReader)
        {
            var dataTable = new DataTable();
            dataTable.Load(dataReader);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(dataTable);
            return JSONString;
        }

        // GET api/<ValuesController>/5
        [HttpGet("productos/{id}")]
        public string Get(int id)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "Select * from PRODUCTOS where IDPRODUCTO = " + id;
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }

        [DataContract]
        public class MyModel
        {
            public string CountryOfOrigin { get; set; }
            public string Commodity { get; set; }
        }

        System.IFormatProvider cultureUS = new System.Globalization.CultureInfo("en-US");

        System.Globalization.CultureInfo cultureFr = new System.Globalization.CultureInfo("fr-fr");


        // POST api/<ValuesController> [FromBody] JsonElement json

        [HttpPost("productos/grabar_markup_particular_de_producto")]
        //[Consumes("application/json")]
        public string grabar_markup_particular_de_producto([FromBody] grabar_markup_particular_de_producto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            var markup = model.MARKUP.ToString().Replace(",", ".");
            try
            {
                conexion.Open();
                string query = "IF NOT EXISTS(SELECT * FROM LISTASPRECIOSDETALLES WHERE IDLISTAPRECIO = " + model.IDLISTAPRECIO + " AND IDPRODUCTO = " + model.IDPRODUCTO + " AND CREADOPOR = '" + model.USUARIO + "' ) " +
                    "INSERT INTO LISTASPRECIOSDETALLES(IDLISTAPRECIODETALLE, IDLISTAPRECIO, IDPRODUCTO, MARKUP, CREADOPOR, PRECIOVENTANETO, PRECIOVENTAFINAL) " +
                    "VALUES((SELECT(MAX(IDLISTAPRECIODETALLE) + 1) FROM LISTASPRECIOSDETALLES)," + model.IDLISTAPRECIO + "," + model.IDPRODUCTO + "," + markup + ",'" + model.USUARIO + "', " + model.NETO + ", " + model.FINAL + ") " +
                    "ELSE UPDATE LISTASPRECIOSDETALLES SET MARKUP = " + markup + ", PRECIOVENTANETO = " + model.NETO + ", PRECIOVENTAFINAL = " + model.FINAL + " WHERE IDLISTAPRECIO = " + model.IDLISTAPRECIO + " AND IDPRODUCTO = " + model.IDPRODUCTO + " AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return query;
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/modificar_markups_de_listas")]
        //[Consumes("application/json")]
        public string modificar_markups_de_listas([FromBody] modificar_markups_de_listas model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            var markup = model.MARKUP.ToString().Replace(",", ".");
            try
            {
                conexion.Open();
                string query = "DECLARE @MYARRAY table (IDPRODUCTO int, ARRAYINDEX int identity(1,1) ) " +
                    "DECLARE @MYARRAY2 table(IDLISTA int, ARRAYINDEX int identity(1, 1)) " +
                    "DECLARE @DEFAULTMARKUPARRAY table(MARKUP int, ARRAYINDEX int identity(1, 1)) " +
                    "INSERT INTO @DEFAULTMARKUPARRAY(MARKUP) VALUES " + model.DEFAULTMARKUPS + " " +
                    "INSERT INTO @MYARRAY(IDPRODUCTO) VALUES " + model.ARRAYPRODS + " " +
                    "INSERT INTO @MYARRAY2(IDLISTA) VALUES " + model.ARRAYLISTASPRECIOS + " " +
                    "DECLARE @INDEXVAR int " +
                    "DECLARE @INDEXVAR2 int " +
                    "DECLARE @TOTALCOUNT int " +
                    "DECLARE @TOTALCOUNT2 int " +
                    "DECLARE @CURINDEXEDPRODUCTID int " +
                    "DECLARE @CURINDEXEDPRODUCTID2 int " +
                    "DECLARE @markup float " +
                    "DECLARE @defaultmarkup float " +
                    "SET @INDEXVAR = 0 " +
                    "SET @INDEXVAR2 = 0 " +
                    "SET @markup = " + markup + " " +
                    "SET @defaultmarkup = 0 " +
                    "SELECT @TOTALCOUNT = COUNT(*) FROM @MYARRAY " +
                    "SELECT @TOTALCOUNT2 = COUNT(*) FROM @MYARRAY2 " +
                    "WHILE @INDEXVAR < @TOTALCOUNT " +
                    "BEGIN " +
                    "SELECT @INDEXVAR = @INDEXVAR + 1 " +
                    "SET @INDEXVAR2 = 0 " +
                    "SELECT @CURINDEXEDPRODUCTID = IDPRODUCTO from @MYARRAY where ARRAYINDEX = @INDEXVAR " +
                    "WHILE @INDEXVAR2 < @TOTALCOUNT2 " +
                    "BEGIN " +
                    "SELECT @INDEXVAR2 = @INDEXVAR2 + 1 " +
                    "SELECT @CURINDEXEDPRODUCTID2 = IDLISTA from @MYARRAY2 where ARRAYINDEX = @INDEXVAR2 " +
                    "SELECT @defaultmarkup = MARKUP from @DEFAULTMARKUPARRAY where ARRAYINDEX = @INDEXVAR2 " +
                    "IF NOT EXISTS(SELECT * FROM LISTASPRECIOSDETALLES WHERE IDLISTAPRECIO = @CURINDEXEDPRODUCTID2 AND IDPRODUCTO = @CURINDEXEDPRODUCTID) " +
                    "INSERT INTO LISTASPRECIOSDETALLES(IDLISTAPRECIODETALLE, IDLISTAPRECIO, IDPRODUCTO, MARKUP) " +
                    "VALUES((SELECT(MAX(IDLISTAPRECIODETALLE) + 1) FROM LISTASPRECIOSDETALLES), @CURINDEXEDPRODUCTID2, @CURINDEXEDPRODUCTID, @defaultmarkup) " +
                    "ELSE " +
                    "UPDATE LISTASPRECIOSDETALLES " +
                    "SET MARKUP = MARKUP * (1 + @markup / 100) WHERE IDLISTAPRECIO = @CURINDEXEDPRODUCTID2 AND IDPRODUCTO = @CURINDEXEDPRODUCTID " +
                    "END " +
                    "END";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }


        [HttpPost("productos/grabar_valores_correlativos")]
        //[Consumes("application/json")]
        public string grabar_valores_correlativos([FromBody] grabar_valores_correlativos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var IVA = model.IVA.ToString().Replace(",", ".");
            var PRECIOCOSTONETO = model.PRECIOCOSTONETO.ToString().Replace(",", ".");
            var PRECIOCOSTOFINAL = model.PRECIOCOSTOFINAL.ToString().Replace(",", ".");
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET PORCENTAJEIVA = " + IVA + ",PRECIOCOSTONETO = " + PRECIOCOSTONETO + ",PRECIOCOSTOFINAL = " + PRECIOCOSTOFINAL + " WHERE IDPRODUCTO = " + model.IDPRODUCTO;
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/modificar_costo_neto_peso")]
        //[Consumes("application/json")]
        public string modificar_costo_neto_peso([FromBody] modificar_costo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET PRECIOCOSTONETO = PRECIOCOSTONETO " + model.OPERACION + " " + model.NUMERO + " WHERE IDPRODUCTO IN(" + model.PRODUCTO + ") AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/modificar_costo_final_peso")]
        //[Consumes("application/json")]
        public string modificar_costo_final_peso([FromBody] modificar_costo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET PRECIOCOSTOFINAL = PRECIOCOSTOFINAL " + model.OPERACION + " " + model.NUMERO + " WHERE IDPRODUCTO IN(" + model.PRODUCTO + ") AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/modificar_costo_neto_porcentaje")]
        //[Consumes("application/json")]
        public string modificar_costo_neto_porcentaje([FromBody] modificar_costo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET PRECIOCOSTONETO = PRECIOCOSTONETO " + model.OPERACION + " ((PRECIOCOSTONETO*" + model.NUMERO + ")/100) WHERE IDPRODUCTO IN(" + model.PRODUCTO + ") AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("productos/modificar_costo_final_porcentaje")]
        //[Consumes("application/json")]
        public string modificar_costo_final_porcentaje([FromBody] modificar_costo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET PRECIOCOSTOFINAL = PRECIOCOSTOFINAL " + model.OPERACION + " ((PRECIOCOSTOFINAL*" + model.NUMERO + ")/100) WHERE IDPRODUCTO IN(" + model.PRODUCTO + ") AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("productos/modificar_precios_en_productos")]
        //[Consumes("application/json")]
        public string modificar_precios_en_productos([FromBody] modificar_precios_en_productos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET " + model.OPERACIONPRECIO + " WHERE IDPRODUCTO IN(" + model.IDS + ")";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }

        [HttpPost("productos/redondear_productos")]
        //[Consumes("application/json")]
        public string redondear_productos([FromBody] redondear_productos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET " + model.CAMPO + " = ROUND(" + model.CAMPO + ",0) WHERE id_usuario = " + model.ID_USUARIO;
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }


        [HttpPost("productos/obtener_info")]
        public string obtener_info([FromBody] obtener_info model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT " +
                    "ISNULL(DESCRIPCION, '') AS DESCRIPCION," +
                    "ISNULL(CODIGO, '') AS CODIGO," +
                    "ISNULL(ALIAS, '') AS ALIAS," +
                    "ISNULL(IDTIPOPRODUCTO, 0) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA," +
                    "ISNULL(VENTAPORSESIONES, 0) AS VENTAPORSESIONES," +
                    "ISNULL(DURACIONTURNO, 0) AS DURACIONTURNO," +
                    "ISNULL(MANEJASTOCK, 0) AS MANEJASTOCK," +
                    "ISNULL(STOCKMINIMO, 0) AS STOCKMINIMO," +
                    "ISNULL(CODIGOBARRAS, '') AS CODIGOBARRAS," +
                    "ISNULL(DESTACADOTIENDA, 0) AS DESTACADOTIENDA," +
                    "ISNULL(PORCENTAJEIVA, 0) AS PORCENTAJEIVA," +
                    "ISNULL(CODIGOQR, '') AS CODIGOQR FROM PRODUCTOS WHERE IDPRODUCTO = " + model.ID_PRODUCTO + " AND CREADOPOR = '" + model.ID_USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/obtener_info_adicionales_modulos_valores")]
        public string obtener_info_adicionales_modulos_valores([FromBody] obtener_info_adicionales_modulos_valores model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT * FROM ADICIONALESMODULOSVALORES WHERE CREADOPOR = '" + model.ID_USUARIO + "' AND IDTABLA = " + model.ID_PRODUCTO + " ORDER BY IDADICIONALMODULO";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/modificar_producto")]
        //[Consumes("application/json")]
        public string modificar_producto([FromBody] modificar_producto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET " + model.CAMPOS_VALORES + " WHERE IDPRODUCTO = " + model.ID_PRODUCTO + ";" + model.ATRIBUTOS;
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }

        [HttpPost("productos/modificar_estado_productos")]
        //[Consumes("application/json")]
        public string modificar_estado_productos([FromBody] modificar_estado_productos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET ACTIVO = " + model.ESTADO + " WHERE IDPRODUCTO IN(" + model.IDS + ")";
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/modificar_en_server")]
        //[Consumes("application/json")]
        public string modificar_en_server([FromBody] modificar_en_server model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET " + model.CAMPO + " = " + model.VALOR + " WHERE IDPRODUCTO = " + model.ID_PRODUCTO;
                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("categorias/nuevo_valor_en_tipo_producto")]
        //[Consumes("application/json")]
        public string nuevo_valor_en_tipo_producto([FromBody] nuevo_valor_en_tipo_producto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "IF(NOT EXISTS(SELECT * FROM TIPOPRODUCTOS WHERE NOMBRE = '" + model.VALOR + "' AND IDPADRE IS NULL AND CREADOPOR = '" + model.ID_USUARIO + "')) " +
                    "BEGIN " +
                    "INSERT INTO TIPOPRODUCTOS(IDTIPOPRODUCTO, IDPADRE, NOMBRE, ACTIVO, CREADOPOR, FECHACREADO) " +
                    "VALUES((SELECT MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS) + 1,NULL,'" + model.VALOR + "',1,'" + model.ID_USUARIO + "',GETDATE()) " +
                    "END;SELECT MAX(IDTIPOPRODUCTO) AS ULTIMOID FROM TIPOPRODUCTOS;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                conexion.Close();
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("listas_precios/crear_lista")]
        //[Consumes("application/json")]
        public string crear_lista([FromBody] crear_lista model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "INSERT INTO LISTASPRECIOS(IDLISTAPRECIO,NOMBRE,ACTIVO,CREADOPOR,FECHACREADO,MARKUP) " +
                    "VALUES((SELECT MAX(IDLISTAPRECIO) FROM LISTASPRECIOS) + 1," +
                    "'" + model.NOMBRE + "'," +
                    "1," +
                    "'" + model.USUARIO + "'," +
                    "GETDATE()," +
                    +model.MARKUP + ");SELECT MAX(IDLISTAPRECIO) AS ULTIMOID FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "';";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                conexion.Close();
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }









        [HttpPost("categorias/nuevo_valor_en_sub_tipo_producto")]
        //[Consumes("application/json")]
        public string nuevo_valor_en_sub_tipo_producto([FromBody] nuevo_valor_en_sub_tipo_producto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "IF(NOT EXISTS(SELECT * FROM TIPOPRODUCTOS WHERE NOMBRE = '" + model.VALOR + "' AND IDPADRE = " + model.ID_PADRE + " AND CREADOPOR = '" + model.ID_USUARIO + "')) " +
                    "BEGIN " +
                    "INSERT INTO TIPOPRODUCTOS(IDTIPOPRODUCTO, IDPADRE, NOMBRE, ACTIVO, CREADOPOR, FECHACREADO) " +
                    "VALUES((SELECT MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS) + 1," + model.ID_PADRE + ",'" + model.VALOR + "',1,'" + model.ID_USUARIO + "',GETDATE()) " +
                    "END;SELECT MAX(IDTIPOPRODUCTO) AS ULTIMOID FROM TIPOPRODUCTOS;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                conexion.Close();
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("productos/obtener_id_por_qr")]
        //[Consumes("application/json")]
        public string obtener_id_por_qr([FromBody] obtener_id_por_qr model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            var values = new List<Dictionary<string, object>>();
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "SELECT IDPRODUCTO FROM PRODUCTOS WHERE CREADOPOR = '" + model.ID_USUARIO + "' AND CODIGOQR = '" + model.CODIGOQR + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/crear_producto")]
        //[Consumes("application/json")]
        public string crear_producto([FromBody] crear_producto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "INSERT INTO PRODUCTOS(IDPRODUCTO,CREADOPOR,IDTIPOPRODUCTO,IDSUBCATEGORIA,VENTAPORSESIONES," +
                    "DURACIONTURNO,CODIGO,DESCRIPCION,ALIAS,PORCENTAJEIVA,PRECIOCOSTONETO,PRECIOCOSTOFINAL,MANEJASTOCK,STOCKMINIMO,CODIGOBARRAS," +
                    "CODIGOQR,DESTACADOTIENDA,FECHACREADO,ACTIVO) VALUES((SELECT MAX(IDPRODUCTO) FROM PRODUCTOS) + 1,'" + model.CREADOPOR + "'," +
                    "" + model.CATEGORIA + "," + model.SUBCATEGORIA + "," + model.PERMITEAGENDAR + "," + model.DURACIONTURNO + ",'" + model.CODIGO + "'," +
                    "'" + model.DESCRIPCION + "','" + model.ALIAS + "'," + model.PORCENTAJEIVA + ",0,0," + model.MANEJASTOCK + "," + model.STOCKMINIMO + "," +
                    "'" + model.CODIGOBARRAS + "','" + model.CODIGOQR + "'," + model.DESTACADO + ",GETDATE(),1);SELECT MAX(IDPRODUCTO) AS ULTIMOID FROM PRODUCTOS;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                conexion.Close();
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }

        [HttpPost("productos/crear_producto_atributos")]
        //[Consumes("application/json")]
        public string crear_producto_atributos([FromBody] crear_producto_atributos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = model.ATRIBUTOS;

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/asociar_atributos_a_productos")]
        //[Consumes("application/json")]
        public string asociar_atributos_a_productos([FromBody] asociar_atributos_a_productos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT IDADICIONALMODULO,DESCRIPCION FROM ADICIONALESMODULOS WHERE CREADOPOR = '" + model.ID_USUARIO + "' AND ACTIVO = 1";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }


        [HttpPost("productos/asociar_atributos_a_productos2")]
        //[Consumes("application/json")]
        public string asociar_atributos_a_productos2([FromBody] asociar_atributos_a_productos_2 model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT * FROM ADICIONALESMODULOSVALORES WHERE " +
                    "CREADOPOR = '" + model.ID_USUARIO + "' AND IDTABLA IN(" + model.PRODUCTOS + ") AND " +
                    "IDADICIONALMODULO IN(" + model.ADICIONALESACTIVOS + ") AND " +
                    "CAST(DESCRIPCION AS VARCHAR) != '' " +
                    "ORDER BY IDTABLA,IDADICIONALMODULO";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }


        [HttpPost("productos/consultar_productos_de_usuario")]
        public string consultar_productos_de_usuario([FromBody] consultar_productos_de_usuario model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO," +
                    "ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0 ) AS IDSUBCATEGORIA," +
                    "ISNULL(CODIGO, '' ) AS CODIGO," +
                    "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE CREADOPOR = '" + model.ID_USUARIO + "' ORDER BY IDPRODUCTO DESC";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("productos/consultar_productos_de_scroll")]
        //[Consumes("application/json")]
        public string consultar_productos_de_scroll2([FromBody] consulta_scroll model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string listas = "";
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo);

            string querystock = "";

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();


                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }


                query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE " +
                    "CREADOPOR = '" + model.USUARIO + "' AND IDPRODUCTO < " + model.PRODUCTO + " ORDER BY IDPRODUCTO DESC";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }









        [HttpPost("productos/consultar_productos_master")]
        //[Consumes("application/json")]
        public string consultar_productos_master([FromBody] consultar_productos_master model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);

            string productos = "";


            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();


                


                string query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO, ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOSMASTER WHERE " +
                    "(SELECT COUNT(*) AS TOTAL FROM PRODUCTOS WHERE CREADOPOR = '"+model.USUARIO+"' AND IMPORTADO = PRODUCTOSMASTER.IDPRODUCTO) = 0";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }           
              


                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }


        [HttpPost("productos/consultar_productos_activos_scroll")]
        //[Consumes("application/json")]
        public string consultar_productos_de_scroll3([FromBody] consulta_scroll model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string listas = "";
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo);

            string querystock = "";

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();


                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }


                query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE " +
                    "CREADOPOR = '"+model.USUARIO+"' AND IDPRODUCTO < "+model.PRODUCTO+" AND ACTIVO = 1 ORDER BY IDPRODUCTO DESC";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("productos/consultar_productos_inactivos_scroll")]
        //[Consumes("application/json")]
        public string consultar_productos_de_scroll4([FromBody] consulta_scroll model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string listas = "";
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo);

            string querystock = "";

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();


                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }


                query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE " +
                    "CREADOPOR = '" + model.USUARIO + "' AND IDPRODUCTO < " + model.PRODUCTO + " AND ACTIVO = 0 ORDER BY IDPRODUCTO DESC";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("productos/productos_con_stock_scroll")]
        //[Consumes("application/json")]
        public string consultar_productos_de_scroll5([FromBody] consulta_scroll model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string listas = "";
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo);

            string querystock = "";

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();


                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }


                query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO, ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO," +
                    "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION, ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0 ) AS ACTIVO, ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS AS P1 WHERE(SELECT COUNT(*) AS TOTAL FROM(SELECT IDPRODUCTO," +
                    "(SELECT ISNULL(SUM(CANTIDADACTUAL), 0) FROM STOCK WHERE IDSUCURSAL IN("+model.SUCURSALES+") AND CREADOPOR = 'felipe' AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL " +
                    "FROM STOCK AS A GROUP BY IDPRODUCTO) T WHERE SUMATOTAL != 0 AND P1.IDPRODUCTO = IDPRODUCTO) != 0 AND P1.IDPRODUCTO < "+model.PRODUCTO+" ORDER BY IDPRODUCTO DESC";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/productos_sin_stock_scroll")]
        //[Consumes("application/json")]
        public string consultar_productos_de_scroll6([FromBody] consulta_scroll model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string listas = "";
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo);

            string querystock = "";

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();


                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }


                query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO, ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO," +
                    "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION, ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0 ) AS ACTIVO, ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS AS P1 WHERE(SELECT COUNT(*) AS TOTAL FROM(SELECT IDPRODUCTO," +
                    "(SELECT ISNULL(SUM(CANTIDADACTUAL), 0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND CREADOPOR = 'felipe' AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL " +
                    "FROM STOCK AS A GROUP BY IDPRODUCTO) T WHERE SUMATOTAL = 0 AND P1.IDPRODUCTO = IDPRODUCTO) != 0 AND P1.IDPRODUCTO < " + model.PRODUCTO + " ORDER BY IDPRODUCTO DESC";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }







        [HttpPost("productos/consultar_buscador")]
        //[Consumes("application/json")]
        public string consultar_buscador([FromBody] consultar_buscador model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string listas = "";
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo);

            string querystock = "";

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();


                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }


                query = "SELECT IDPRODUCTO,IDTIPOPRODUCTO,IDSUBCATEGORIA,CODIGO,DESCRIPCION,PORCENTAJEIVA,ACTIVO,PRECIOCOSTONETO,PRECIOCOSTOFINAL " +
                    "FROM( " +
                    "SELECT " +
                    "ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO," +
                    "ISNULL(IDTIPOPRODUCTO, 0) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA," +
                    "ISNULL(CODIGO, '') AS CODIGO," +
                    "ISNULL(DESCRIPCION, '') AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0) AS PRECIOCOSTOFINAL," +
                    "ISNULL((SELECT NOMBRE FROM TIPOPRODUCTOS WHERE CREADOPOR = '"+model.USUARIO+"' AND TIPOPRODUCTOS.IDTIPOPRODUCTO = PRODUCTOS.IDTIPOPRODUCTO), '') AS CATEGORIA," +
                    "ISNULL((SELECT NOMBRE FROM TIPOPRODUCTOS WHERE CREADOPOR = '" + model.USUARIO + "' AND TIPOPRODUCTOS.IDTIPOPRODUCTO = PRODUCTOS.IDSUBCATEGORIA),'') AS SUBCATEGORIA," +
                    "(SELECT COUNT(*) AS ADSAS FROM (" +
                    "SELECT " +
                    "CAST(ISNULL(MARKUP, 0) as varchar(10)) AS MARKUP,CAST(ISNULL(PRECIOVENTANETO, 0) as varchar(10)) AS PRECIOVENTANETO," +
                    "CAST(ISNULL(PRECIOVENTAFINAL, 0) as varchar(10)) AS PRECIOVENTAFINAL " +
                    "FROM LISTASPRECIOSDETALLES WHERE CREADOPOR = '" + model.USUARIO + "' AND IDPRODUCTO = PRODUCTOS.IDPRODUCTO AND " +
                    "(MARKUP LIKE '%" + model.BUSQUEDA + "%' OR PRECIOVENTANETO LIKE '%" + model.BUSQUEDA + "%' OR PRECIOVENTAFINAL LIKE '%" + model.BUSQUEDA + "%')) T ) AS ALGUNPRECIO " +
                    "FROM PRODUCTOS WHERE CREADOPOR = '" + model.USUARIO + "') T2 WHERE (" +
                    "CODIGO LIKE '%" + model.BUSQUEDA + "%' OR " +
                    "DESCRIPCION LIKE '%" + model.BUSQUEDA + "%' OR " +
                    "PORCENTAJEIVA LIKE '%" + model.BUSQUEDA + "%' OR " +
                    "PRECIOCOSTONETO LIKE '%" + model.BUSQUEDA + "%' OR " +
                    "PRECIOCOSTOFINAL LIKE '%" + model.BUSQUEDA + "%' OR " +
                    "CATEGORIA LIKE '%" + model.BUSQUEDA + "%' OR " +
                    "SUBCATEGORIA LIKE '%" + model.BUSQUEDA + "%' OR " +
                    "ALGUNPRECIO != 0) ORDER BY IDPRODUCTO DESC";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }













        [HttpPost("productos/consultar_ids_productos_de_usuario")]
        public string consultar_ids_productos_de_usuario([FromBody] consultar_productos_de_usuario model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO, " +
                    "ISNULL(PORCENTAJEIVA,0) AS PORCENTAJEIVA, " +
                    "ISNULL(PRECIOCOSTONETO,0) AS PRECIOCOSTONETO, " +
                    "ISNULL(PRECIOCOSTOFINAL,0) AS PRECIOCOSTOFINAL " +
                    "FROM PRODUCTOS WHERE CREADOPOR = '" + model.ID_USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/exportar_productos_de_usuario")]
        public string exportar_productos_de_usuario([FromBody] exportar_productos_de_usuario model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT TOP 50 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO," +
                   "ISNULL(IDTIPOPRODUCTO, 0) AS IDTIPOPRODUCTO," +
                   "ISNULL(IDSUBCATEGORIA, 0 ) AS IDSUBCATEGORIA," +
                   "ISNULL(VENTAPORSESIONES, 0 ) AS VENTAPORSESIONES," +
                   "ISNULL(MANEJASTOCK, 0 ) AS MANEJASTOCK, " +
                   "ISNULL(DESTACADOTIENDA, 0 ) AS DESTACADOTIENDA," +
                   "ISNULL(STOCKMINIMO, 0 ) AS STOCKMINIMO," +
                   "ISNULL(CODIGO, '' ) AS CODIGO," +
                   "ISNULL(CODIGOBARRAS, '' ) AS CODIGOBARRAS," +
                   "ISNULL(ALIAS, '' ) AS ALIAS," +
                   "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                   "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                   "ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                   "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                   "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE " +
                   "CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/exportar_productos_puntuales_de_usuario")]
        public string exportar_productos_puntuales_de_usuario([FromBody] exportar_productos_puntuales_de_usuario model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO," +
                   "ISNULL(IDTIPOPRODUCTO, 0) AS IDTIPOPRODUCTO," +
                   "ISNULL(IDSUBCATEGORIA, 0 ) AS IDSUBCATEGORIA," +
                   "ISNULL(VENTAPORSESIONES, 0 ) AS VENTAPORSESIONES," +
                   "ISNULL(MANEJASTOCK, 0 ) AS MANEJASTOCK, " +
                   "ISNULL(DESTACADOTIENDA, 0 ) AS DESTACADOTIENDA," +
                   "ISNULL(STOCKMINIMO, 0 ) AS STOCKMINIMO," +
                   "ISNULL(CODIGO, '' ) AS CODIGO," +
                   "ISNULL(CODIGOBARRAS, '' ) AS CODIGOBARRAS," +
                   "ISNULL(ALIAS, '' ) AS ALIAS," +
                   "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                   "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                   "ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                   "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                   "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE " +
                   "CREADOPOR = '" + model.USUARIO + "' AND IDPRODUCTO IN(" + model.PRODUCTOS + ")";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/consultar_categorias")]
        public string consultar_categorias([FromBody] consultar_categorias model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDTIPOPRODUCTO,0) AS IDTIPOPRODUCTO,ISNULL(NOMBRE,'') AS NOMBRE FROM TIPOPRODUCTOS WHERE IDPADRE IS NULL AND ACTIVO = 1 AND CREADOPOR = '" + model.ID_USUARIO + "' ORDER BY NOMBRE";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/consultar_subcategorias")]
        public string consultar_subcategorias([FromBody] consultar_subcategorias model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDTIPOPRODUCTO,0) AS IDTIPOPRODUCTO,ISNULL(NOMBRE,'') AS NOMBRE,ISNULL(IDPADRE,0) AS IDPADRE FROM TIPOPRODUCTOS WHERE ACTIVO = 1 AND IDPADRE IS NOT NULL AND CREADOPOR = '" + model.ID_USUARIO + "' ORDER BY NOMBRE";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/consulta_markups_particulares")]
        public string consulta_markups_particulares([FromBody] consulta_markups_particulares model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                    "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                    "FROM LISTASPRECIOSDETALLES WHERE " +
                    "IDLISTAPRECIO IN(" + model.LISTAS_PRECIOS + ") AND " +
                    "IDPRODUCTO IN(" + model.PRODUCTOS + ") AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("listas_precios/consulta_markups_generales")]
        public string consulta_markups_generales([FromBody] consulta_markups_generales model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO, ISNULL(MARKUP,0) AS MARKUP FROM LISTASPRECIOS WHERE ACTIVO = 1 AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }


        [HttpPost("productos/consulta_costos_netos")]
        public string consulta_costos_netos([FromBody] consulta_costos_netos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDPRODUCTO,0) AS IDPRODUCTO,ISNULL(PRECIOCOSTONETO,0) AS COSTONETO,ISNULL(PORCENTAJEIVA,0) AS IVA FROM PRODUCTOS WHERE IDPRODUCTO IN(" + model.PRODUCTOS + ") AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/consulta_costos_finales")]
        public string consulta_costos_finales([FromBody] consulta_costos_netos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDPRODUCTO,0) AS IDPRODUCTO,ISNULL(PRECIOCOSTOFINAL,0) AS COSTOFINAL FROM PRODUCTOS WHERE IDPRODUCTO IN(" + model.PRODUCTOS + ") AND CREADOPOR = '" + model.USUARIO + "'";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/consultar_atributos")]
        public string consultar_atributos([FromBody] consultar_atributos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDADICIONALMODULO,0) AS IDADICIONALMODULO, ISNULL(ESTILO,'') AS ESTILO, ISNULL(DESCRIPCION,'') AS DESCRIPCION, ISNULL(IDENTIFICADOR,'') AS IDENTIFICADOR FROM ADICIONALESMODULOS WHERE CREADOPOR = '" + model.ID_USUARIO + "' AND ACTIVO = 1 ORDER BY DESCRIPCION";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/consultar_atributos_combos")]
        public string consultar_atributos_combos([FromBody] consultar_atributos_combos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDADICIONALMODULO,0) AS IDADICIONALMODULO, ISNULL(IDIN,0) AS IDIN, ISNULL(VALOR,'') AS VALOR FROM ADICIONALESMODULOSCOMBOS WHERE IDADICIONALMODULO IN(" + model.ATRIBUTOS + ") AND ACTIVO = 1 ORDER BY VALOR";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/consulta_stock")]
        public string consulta_stock([FromBody] consulta_stock model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = model.CONSULTA;
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("atributos/crear_atributo_seleccion")]
        public string crear_atributo_seleccion([FromBody] crear_atributo_seleccion model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SET IDENTITY_INSERT ADICIONALESMODULOS ON;" +
                    "INSERT INTO ADICIONALESMODULOS (IDADICIONALMODULO,GRUPO,IDENTIFICADOR,DESCRIPCION,ACTIVO,OBLIGATORIO,ESTILO,CREADOPOR,FECHACREADO) " +
                    "VALUES(((SELECT MAX(IDADICIONALMODULO) FROM ADICIONALESMODULOS)+1),'PRODUCTOS','" + model.IDENTIFICADOR + "'," +
                    "'" + model.DESCRIPCION + "',1,0,'" + model.ESTILO + "','" + model.ID_USUARIO + "',GETDATE());" +
                    "SELECT MAX(IDADICIONALMODULO) as ULTIMOID FROM ADICIONALESMODULOS;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("atributos/crear_atributo_seleccion_combos")]
        //[Consumes("application/json")]
        public string crear_atributo_seleccion_combos([FromBody] crear_atributo_seleccion_combos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = model.CONSULTA;

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("atributos/crear_atributo_texto")]
        //[Consumes("application/json")]
        public string crear_atributo_texto([FromBody] crear_atributo_texto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SET IDENTITY_INSERT ADICIONALESMODULOS ON;" +
                    "INSERT INTO ADICIONALESMODULOS (IDADICIONALMODULO,GRUPO,IDENTIFICADOR,DESCRIPCION,ACTIVO,OBLIGATORIO,ESTILO,CREADOPOR,FECHACREADO) " +
                    "VALUES(((SELECT MAX(IDADICIONALMODULO) FROM ADICIONALESMODULOS)+1),'PRODUCTOS','" + model.IDENTIFICADOR + "','" + model.DESCRIPCION + "',1,0," +
                    "'" + model.ESTILO + "','" + model.ID_USUARIO + "',GETDATE());" +
                    "SELECT MAX(IDADICIONALMODULO) as ULTIMOID FROM ADICIONALESMODULOS;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("atributos/deshabilitar_atributo")]
        //[Consumes("application/json")]
        public string deshabilitar_atributo([FromBody] deshabilitar_atributo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE ADICIONALESMODULOS SET ACTIVO = 0 WHERE IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND CREADOPOR = '" + model.ID_USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("atributos/modificar_identificador")]
        //[Consumes("application/json")]
        public string modificar_identificador([FromBody] modificar_identificador model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE ADICIONALESMODULOS SET IDENTIFICADOR = '" + model.IDENTIFICADOR + "' WHERE " +
                    "IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND " +
                    "CREADOPOR = '" + model.ID_USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("listas_precios/grabar_markups_inexistentes")]
        //[Consumes("application/json")]
        public string grabar_markups_inexistentes([FromBody] grabar_markups_inexistentes model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = model.CONSULTA;

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("atributos/consultar_identificador")]
        //[Consumes("application/json")]
        public string consultar_identificador([FromBody] consultar_identificador model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT IDENTIFICADOR FROM ADICIONALESMODULOS WHERE IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND CREADOPOR = '" + model.ID_USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("atributos/cambiar_estilo_atributo")]
        //[Consumes("application/json")]
        public string cambiar_estilo_atributo([FromBody] cambiar_estilo_atributo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE ADICIONALESMODULOS SET ESTILO = '" + model.ESTILO + "' WHERE IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND CREADOPOR = '" + model.ID_USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("atributos/insertar_combo_en_atributo")]
        //[Consumes("application/json")]
        public string insertar_combo_en_atributo([FromBody] insertar_combo_en_atributo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "BEGIN IF EXISTS(SELECT* FROM ADICIONALESMODULOS WHERE IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND CREADOPOR = '" + model.ID_USUARIO + "') " +
                    "BEGIN " +
                        "SET IDENTITY_INSERT ADICIONALESMODULOSCOMBOS ON;" +
                        "INSERT INTO ADICIONALESMODULOSCOMBOS(IDIN, IDADICIONALMODULO, VALOR, ACTIVO) VALUES((SELECT MAX(IDIN) FROM ADICIONALESMODULOSCOMBOS) + 1," + model.ID_ATRIBUTO + ",'" + model.VALOR + "',1) " +
                    "END " +
                    "END";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("atributos/consultar_combos_atributo")]
        //[Consumes("application/json")]
        public string consultar_combos_atributo([FromBody] consultar_combos_atributo model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT * FROM ADICIONALESMODULOSCOMBOS WHERE " +
                    "IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND " +
                    "(SELECT COUNT(*) FROM ADICIONALESMODULOS WHERE IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND CREADOPOR = '" + model.ID_USUARIO + "') > 0 AND ACTIVO = 1";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/consultar_productos_activos")]
        //[Consumes("application/json")]
        public string consultar_productos_activos([FromBody] consultar_productos_activos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();

            string listas = "";
            string productos = "";

            var pInfo1 = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo1);

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();

            try
            {
                conexion.Open();
                string query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE ACTIVO = 1 AND CREADOPOR = '" + model.USUARIO + "' ORDER BY IDPRODUCTO DESC";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            valuesgeneral["Productos"].Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }



                query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                string querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }






                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("productos/consultar_productos_con_stock")]
        //[Consumes("application/json")]
        public string consultar_productos_con_stock([FromBody] consultar_productos_con_stock model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();

            string listas = "";
            string productos = "";

            var pInfo1 = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo1);

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();

            try
            {
                conexion.Open();
                string query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO, ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO," +
                    "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION, ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0 ) AS ACTIVO, ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS AS P1 WHERE(SELECT COUNT(*) AS TOTAL FROM(SELECT IDPRODUCTO," +
                    "(SELECT ISNULL(SUM(CANTIDADACTUAL), 0) FROM STOCK WHERE IDSUCURSAL IN("+model.SUCURSALES+") AND CREADOPOR = '"+model.USUARIO+"' AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL " +
                    "FROM STOCK AS A GROUP BY IDPRODUCTO) T WHERE SUMATOTAL != 0 AND P1.IDPRODUCTO = IDPRODUCTO) != 0 ORDER BY IDPRODUCTO DESC";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            valuesgeneral["Productos"].Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }



                query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                string querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }






                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("productos/consultar_productos_sin_stock")]
        //[Consumes("application/json")]
        public string consultar_productos_sin_stock([FromBody] consultar_productos_con_stock model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();

            string listas = "";
            string productos = "";

            var pInfo1 = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo1);

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();

            try
            {
                conexion.Open();
                string query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO, ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO," +
                    "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION, ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0 ) AS ACTIVO, ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS AS P1 WHERE(SELECT COUNT(*) AS TOTAL FROM(SELECT IDPRODUCTO," +
                    "(SELECT ISNULL(SUM(CANTIDADACTUAL), 0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND CREADOPOR = '" + model.USUARIO + "' AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL " +
                    "FROM STOCK AS A GROUP BY IDPRODUCTO) T WHERE SUMATOTAL = 0 AND P1.IDPRODUCTO = IDPRODUCTO) != 0 ORDER BY IDPRODUCTO DESC";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            valuesgeneral["Productos"].Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }



                query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                string querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }






                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("productos/consultar_productos_inactivos")]
        //[Consumes("application/json")]
        public string consultar_productos_inactivos([FromBody] consultar_productos_activos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();

            string listas = "";
            string productos = "";

            var pInfo1 = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo1);

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();

            try
            {
                conexion.Open();
                string query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE ACTIVO = 0 AND CREADOPOR = '" + model.USUARIO + "' ORDER BY IDPRODUCTO DESC";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            valuesgeneral["Productos"].Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }



                query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                string querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }






                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("categorias/deshabilitarcategoria")]
        //[Consumes("application/json")]
        public string deshabilitarcategoria([FromBody] deshabilitarcategoria model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE TIPOPRODUCTOS SET ACTIVO = 0 WHERE IDTIPOPRODUCTO IN(" + model.CATEGORIAS + ") AND CREADOPOR = '" + model.ID_USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("categorias/modificarcategoria")]
        //[Consumes("application/json")]
        public string modificarcategoria([FromBody] modificarcategoria model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE TIPOPRODUCTOS SET NOMBRE = '" + model.NOMBRE + "' WHERE IDTIPOPRODUCTO = " + model.ID_CATEGORIA + " AND CREADOPOR = '" + model.ID_USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("productos/redondear_costo_neto")]
        //[Consumes("application/json")]
        public string redondear_costo_neto([FromBody] redondear_costo_neto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET PRECIOCOSTONETO = FLOOR(PRECIOCOSTONETO + 0.5) WHERE IDPRODUCTO IN(" + model.PRODUCTOS + ") AND CREADOPOR = '" + model.USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("productos/redondear_costo_final")]
        //[Consumes("application/json")]
        public string redondear_costo_final([FromBody] redondear_costo_neto model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE PRODUCTOS SET PRECIOCOSTOFINAL = FLOOR(PRECIOCOSTOFINAL + 0.5) WHERE IDPRODUCTO IN(" + model.PRODUCTOS + ") AND CREADOPOR = '" + model.USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("listas_precios/redondear_markup")]
        [Consumes("application/json")]
        public string redondear_markup([FromBody] lista_redondear_markups model)
        {

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var modelos = JsonConvert.DeserializeObject<List<redondear_markups>>(pInfo);

            string connString = ConexionString;
            DataTable dt = ToDataTable(modelos);
            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand command = new SqlCommand("", conn))
                {
                    try
                    {
                        conn.Open();

                        //Creating temp table on database
                        command.CommandText = "CREATE TABLE MARKUPSTEMPORALES(LISTAPRECIO numeric(9),PRODUCTO numeric(9),MARKUP decimal(9))";
                        command.ExecuteNonQuery();

                        //Bulk insert into temp table
                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                        {
                            bulkcopy.BulkCopyTimeout = 6600;
                            bulkcopy.DestinationTableName = "MARKUPSTEMPORALES";
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }

                        // Updating destination table, and dropping temp table
                        command.CommandTimeout = 3000;
                        command.CommandText = "UPDATE LISTASPRECIOSDETALLES SET MARKUP = MARKUPSTEMPORALES.MARKUP FROM MARKUPSTEMPORALES WHERE " +
                            "LISTASPRECIOSDETALLES.IDPRODUCTO = MARKUPSTEMPORALES.PRODUCTO AND LISTASPRECIOSDETALLES.IDLISTAPRECIO = MARKUPSTEMPORALES.LISTAPRECIO AND LISTASPRECIOSDETALLES.CREADOPOR='" + model.USUARIO + "'; DROP TABLE MARKUPSTEMPORALES;";
                        command.ExecuteNonQuery();
                        return "Exito";
                    }
                    catch (Exception ex)
                    {
                        return JsonConvert.SerializeObject(ex);
                        // Handle exception properly
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }


        }






        [HttpPost("listas_precios/modificar_markup")]
        [Consumes("application/json")]
        public string modificar_markup([FromBody] lista_modificar_precios model)
        {

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var modelos = JsonConvert.DeserializeObject<List<modificar_precios>>(pInfo);

            string connString = ConexionString;
            DataTable dt = ToDataTable(modelos);
            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand command = new SqlCommand("", conn))
                {
                    try
                    {
                        conn.Open();

                        //Creating temp table on database
                        command.CommandText = "CREATE TABLE MARKUPSTEMPORALES(LISTAPRECIO numeric(9),PRODUCTO numeric(9),MARKUP decimal(9),NETO decimal(9),FINAL decimal(9))";
                        command.ExecuteNonQuery();

                        //Bulk insert into temp table
                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                        {
                            bulkcopy.BulkCopyTimeout = 6600;
                            bulkcopy.DestinationTableName = "MARKUPSTEMPORALES";
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }

                        // Updating destination table, and dropping temp table
                        command.CommandTimeout = 3000;
                        command.CommandText = "UPDATE LISTASPRECIOSDETALLES SET MARKUP = MARKUPSTEMPORALES.MARKUP, PRECIOVENTANETO = MARKUPSTEMPORALES.NETO, PRECIOVENTAFINAL = MARKUPSTEMPORALES.FINAL FROM MARKUPSTEMPORALES WHERE " +
                            "LISTASPRECIOSDETALLES.IDPRODUCTO = MARKUPSTEMPORALES.PRODUCTO AND LISTASPRECIOSDETALLES.IDLISTAPRECIO = MARKUPSTEMPORALES.LISTAPRECIO AND LISTASPRECIOSDETALLES.CREADOPOR='" + model.USUARIO + "'; DROP TABLE MARKUPSTEMPORALES;";
                        command.ExecuteNonQuery();
                        return "Exito";
                    }
                    catch (Exception ex)
                    {
                        return JsonConvert.SerializeObject(ex);
                        // Handle exception properly
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }


        }






        [HttpPost("categorias/grabarcategoria")]
        //[Consumes("application/json")]
        public string grabarcategoria([FromBody] grabarcategoria model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "INSERT INTO TIPOPRODUCTOS (IDTIPOPRODUCTO,IDPADRE,NOMBRE,CREADOPOR,ACTIVO) " +
                    "VALUES((SELECT MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS)+1,null,'" + model.NOMBRE + "','" + model.ID_USUARIO + "',1);" +
                    "SELECT MAX(IDTIPOPRODUCTO) as ULTIMOID FROM TIPOPRODUCTOS";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }


        [HttpPost("categorias/grabarsubcategoria")]
        //[Consumes("application/json")]
        public string grabarsubcategoria([FromBody] grabarsubcategoria model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "INSERT INTO TIPOPRODUCTOS (IDTIPOPRODUCTO,IDPADRE,NOMBRE,CREADOPOR,ACTIVO) " +
                    "VALUES((SELECT MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS)+1," + model.PADRE + ",'" + model.NOMBRE + "','" + model.ID_USUARIO + "',1);" +
                    "SELECT MAX(IDTIPOPRODUCTO) as ULTIMOID FROM TIPOPRODUCTOS";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("categorias/obtener_sub_cats")]
        //[Consumes("application/json")]
        public string obtener_sub_cats([FromBody] obtener_sub_cats model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT IDTIPOPRODUCTO,NOMBRE FROM TIPOPRODUCTOS WHERE IDPADRE = " + model.ID_CATEGORIA + " AND ACTIVO = 1 AND CREADOPOR = '" + model.ID_USUARIO + "' ORDER BY NOMBRE";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("categorias/buscar_productos_con_categoria_subcategoria")]
        //[Consumes("application/json")]
        public string buscar_productos_con_categoria_subcategoria([FromBody] buscar_productos_con_categoria_subcategoria model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT IDPRODUCTO FROM PRODUCTOS WHERE IDTIPOPRODUCTO = " + model.CATEGORIA + " AND IDSUBCATEGORIA = " + model.SUBCATEGORIA + " AND CREADOPOR='" + model.USUARIO + "' AND ACTIVO = 1;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("categorias/buscar_productos_con_categoria")]
        //[Consumes("application/json")]
        public string buscar_productos_con_categoria([FromBody] buscar_productos_con_categoria model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT IDPRODUCTO FROM PRODUCTOS WHERE IDTIPOPRODUCTO = " + model.CATEGORIA + " AND CREADOPOR='" + model.USUARIO + "' AND ACTIVO = 1;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("listas_de_precios/consultar_listas_de_usuario")]
        //[Consumes("application/json")]
        public string consultar_listas_de_usuario([FromBody] consultar_listas_de_usuario model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("productos/insertar_producto_master")]
        //[Consumes("application/json")]
        public string insertar_producto_master([FromBody] insertar_producto_master model)
        {
            string connString = ConexionString;
            string queryfinal = "";
            string categoriamaster = "";
            string subcategoriamaster = "";
            string master = "";
            string productonuevo = "";
            int sepuedeimportar = 0;
            string queryimportacionatributos = "DECLARE @CACHENOMBRECATEGORIA VARCHAR(100) = '';DECLARE @CACHEIDATRIBUTO int = 0;";
            string queryinsert = "INSERT INTO PRODUCTOS (IDPRODUCTO,CREADOPOR,IMPORTADO,";
            string queryvalues = " VALUES((SELECT MAX(IDPRODUCTO) FROM PRODUCTOS)+1,'"+model.USUARIO+"',"+model.PRODUCTOMASTER+",";
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            var values20 = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT * FROM PRODUCTOSMASTER WHERE IDPRODUCTO = " + model.PRODUCTOMASTER + " AND (SELECT COUNT(*) AS TOTAL FROM PRODUCTOS " +
                    "WHERE CREADOPOR = '" + model.USUARIO + "' AND IMPORTADO = " + model.PRODUCTOMASTER + ") = 0";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary     
                            sepuedeimportar = reader.FieldCount;
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) != "IDPRODUCTO" && reader.GetName(i) != "CREADOPOR")
                                {
                                    queryinsert += reader.GetName(i) + ",";
                                    if (reader[i] is string)
                                    {
                                        queryvalues += "'" + reader[i] + "',";
                                    }
                                    else
                                    {
                                        queryvalues += reader[i] + ",";
                                    }
                                    queryvalues += "";
                                }
                                if (reader.GetName(i) == "IDTIPOPRODUCTO")
                                {
                                    categoriamaster = reader[i].ToString();
                                }
                                if (reader.GetName(i) == "CREADOPOR")
                                {
                                    master = reader[i].ToString();
                                }
                                if (reader.GetName(i) == "IDSUBCATEGORIA")
                                {
                                    subcategoriamaster = reader[i].ToString();
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }

                if (queryinsert.Length != 0) { queryinsert = queryinsert.Substring(0, queryinsert.Length - 1); }
                if (queryvalues.Length != 0) { queryvalues = queryvalues.Substring(0, queryvalues.Length - 1); }

                queryfinal = queryinsert + ")" + queryvalues + ");SELECT ISNULL(MAX(IDPRODUCTO),0) AS ULTIMOID FROM PRODUCTOS WHERE IMPORTADO = 1 AND CREADOPOR='"+model.USUARIO+"';";

                // return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            conexion.Close();

            string queryimportarcats = "";

            if (queryfinal != "" && sepuedeimportar != 0)
            {
                
                try
                {
                    conexion.Open();
                    string query = queryfinal;

                    SqlCommand cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "ULTIMOID")
                                    {
                                        productonuevo = reader[i].ToString();
                                    }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                values.Add(fieldValues);

                            }
                        } while (reader.NextResult());
                    }
                    //return JsonConvert.SerializeObject(values);
                }
                catch (SqlException ex)
                {
                    return JsonConvert.SerializeObject(ex);
                }
                catch (Exception ex)
                {
                    return JsonConvert.SerializeObject(ex);
                }



                

                // VAMOS A IMPORTAR LA CATEGORIA Y SUBCATEGORIA MAESTRA Y DESPUES ACTUALIZARLO CON LOS NUEVOS IDS

                if (productonuevo != "" && categoriamaster != "" && subcategoriamaster != "")
                {            

                    try
                    {
                        //conexion.Open();
                        queryimportarcats = "DECLARE @YAEXISTENLASCATEGORIAS VARCHAR(100) = '';" +
                            "DECLARE @YAEXISTENLASSUBCATEGORIAS VARCHAR(100) = '';" +
                            "DECLARE @ELCLIENTEYATIENELACAT VARCHAR(100) = 'No';" +
                            "DECLARE @ELCLIENTEYATIENELASUBCAT VARCHAR(100) = 'No';" +
                            "DECLARE @IDCATEGORIAMASTERENCLIENTE int = 0;" +
                            "DECLARE @IDSUBCATEGORIAMASTERENCLIENTE int = 0;" +
                            "SELECT @YAEXISTENLASCATEGORIAS = NOMBRE FROM TIPOPRODUCTOS WHERE IDTIPOPRODUCTO IN(" + categoriamaster + ") AND IDPADRE IS NULL AND CREADOPOR = '" + master + "';" +
                            "SELECT @YAEXISTENLASSUBCATEGORIAS = NOMBRE FROM TIPOPRODUCTOS WHERE IDTIPOPRODUCTO IN(" + subcategoriamaster + ") AND IDPADRE = " + categoriamaster + " AND CREADOPOR = '" + master + "';" +
                            "IF @YAEXISTENLASCATEGORIAS != '' " +
                            "   SELECT @IDCATEGORIAMASTERENCLIENTE = IDTIPOPRODUCTO FROM TIPOPRODUCTOS WHERE NOMBRE = @YAEXISTENLASCATEGORIAS AND IDPADRE IS NULL AND CREADOPOR = '" + model.USUARIO + "';" +
                            "   IF @IDCATEGORIAMASTERENCLIENTE != 0 " +
                            "       SET @ELCLIENTEYATIENELACAT = 'Si';" +
                            "       SELECT @IDSUBCATEGORIAMASTERENCLIENTE = IDTIPOPRODUCTO FROM TIPOPRODUCTOS WHERE NOMBRE = @YAEXISTENLASSUBCATEGORIAS AND IDPADRE = @IDCATEGORIAMASTERENCLIENTE AND CREADOPOR = '" + model.USUARIO + "';" +
                            "       IF @IDSUBCATEGORIAMASTERENCLIENTE != 0 " +
                            "           SET @ELCLIENTEYATIENELASUBCAT = 'Si';" +
                            "DECLARE @N VARCHAR(100) = '';DECLARE @ID int = 0;DECLARE @N2 VARCHAR(100) = '';DECLARE @ID2 int = 0;" +
                            "IF @ELCLIENTEYATIENELACAT = 'No' AND @ELCLIENTEYATIENELASUBCAT = 'No' " +
                            "   SELECT @N = NOMBRE FROM TIPOPRODUCTOS WHERE IDTIPOPRODUCTO IN(" + categoriamaster + ") AND IDPADRE IS NULL AND CREADOPOR = '" + master + "';" +
                            "   IF @N != '' " +
                            "       BEGIN " +
                            "           INSERT INTO TIPOPRODUCTOS(IDTIPOPRODUCTO, IDPADRE, NOMBRE, ACTIVO, CREADOPOR) " +
                            "           VALUES((SELECT MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS) + 1,NULL,@N,1,'" + model.USUARIO + "');" +
                            "           SELECT @ID = MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS; " +
                            "       END" +
                            "       SELECT @N2 = NOMBRE FROM TIPOPRODUCTOS WHERE IDTIPOPRODUCTO IN(" + subcategoriamaster + ") AND IDPADRE = " + categoriamaster + " AND CREADOPOR = '" + master + "';" +
                            "       IF @N2 != '' " +
                            "           BEGIN " +
                            "           INSERT INTO TIPOPRODUCTOS(IDTIPOPRODUCTO, IDPADRE, NOMBRE, ACTIVO, CREADOPOR) " +
                            "           VALUES((SELECT MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS) + 1,@ID,@N2,1,'" + model.USUARIO + "');" +
                            "           SELECT @ID2 = MAX(IDTIPOPRODUCTO) FROM TIPOPRODUCTOS; " +
                            "           END " +
                            "UPDATE PRODUCTOS SET IDTIPOPRODUCTO = @ID, IDSUBCATEGORIA = @ID2 WHERE IDPRODUCTO = " + productonuevo + " AND CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1;" +
                            "SELECT @ELCLIENTEYATIENELACAT AS EXISTELACAT,@ELCLIENTEYATIENELASUBCAT AS EXISTELASUBCAT,@ID AS CATEGORIA,@ID2 AS SUBCATEGORIA;"
                            ;

                        SqlCommand cmd = new SqlCommand(queryimportarcats, conexion);
                        //using (SqlDataReader reader = cmd.ExecuteReader())
                        cmd.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        return JsonConvert.SerializeObject(ex);
                    }
                    catch (Exception ex)
                    {
                        return JsonConvert.SerializeObject(ex);
                    }


                    conexion.Close();
                }


            }

            

            queryimportacionatributos += "DECLARE @IDPRODUCTOIMPORTADO int = "+productonuevo+"; ";


            if (productonuevo != "")
            {
                try
                {
                    conexion.Open();
                    string query = "SELECT * FROM(" +
                        "SELECT IDADICIONALMODULO, IDTABLA, DESCRIPCION,(SELECT ISNULL(DESCRIPCION, '') AS DESCRIPCION FROM ADICIONALESMODULOS " +
                        "WHERE IDADICIONALMODULO = ADICIONALESMODULOSVALORES.IDADICIONALMODULO) AS NOMBREATRIBUTO," +
                        "(SELECT ISNULL(ESTILO, '') AS DESCRIPCION FROM ADICIONALESMODULOS " +
                        "WHERE IDADICIONALMODULO = ADICIONALESMODULOSVALORES.IDADICIONALMODULO) AS ESTILOATRIBUTO FROM ADICIONALESMODULOSVALORES " +
                        "WHERE IDTABLA = " + model.PRODUCTOMASTER + " AND CREADOPOR = '" + master + "' AND " +
                        "(SELECT COUNT(*) AS TOTAL FROM ADICIONALESMODULOS WHERE CREADOPOR = '" + master + "' AND ACTIVO = 1 AND " +
                        "IDADICIONALMODULO = ADICIONALESMODULOSVALORES.IDADICIONALMODULO) != 0) t " +
                        "WHERE(SELECT COUNT(*) AS TOTAL FROM ADICIONALESMODULOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 AND DESCRIPCION = t.NOMBREATRIBUTO) = 0";

                    SqlCommand cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {

                                queryimportacionatributos += "SET IDENTITY_INSERT ADICIONALESMODULOSCOMBOS OFF;SET IDENTITY_INSERT ADICIONALESMODULOSVALORES OFF;SET IDENTITY_INSERT ADICIONALESMODULOS ON;";
                                queryimportacionatributos += "INSERT INTO ADICIONALESMODULOS(IDADICIONALMODULO,IDENTIFICADOR,DESCRIPCION,ACTIVO,ESTILO,CREADOPOR,FECHACREADO) " +
                                        "VALUES((SELECT MAX(IDADICIONALMODULO) FROM ADICIONALESMODULOS) + 1,'PRODUCTO-' + '" + reader[3] + "','" + reader[3] + "',1,'" + reader[4] + "','" + model.USUARIO + "',GETDATE());SELECT @CACHEIDATRIBUTO = MAX(IDADICIONALMODULO) FROM ADICIONALESMODULOS;";

                                if (reader[4].ToString() == "SELECCION")
                                {
                                    try
                                    {
                                        queryimportacionatributos += "SET IDENTITY_INSERT ADICIONALESMODULOS OFF;SET IDENTITY_INSERT ADICIONALESMODULOSVALORES OFF;SET IDENTITY_INSERT ADICIONALESMODULOSCOMBOS ON;";
                                        SqlConnection conexion5 = new SqlConnection(connString);
                                        conexion5.Open();
                                        string query7 = "SELECT ISNULL(IDIN,0) AS IDIN, ISNULL(IDADICIONALMODULO,0) AS IDADICIONALMODULO, ISNULL(VALOR,'') AS VALOR," +
                                            "ISNULL(ACTIVO, 0) AS ACTIVO FROM ADICIONALESMODULOSCOMBOS WHERE IDADICIONALMODULO = " + reader[0] + "; ";

                                        SqlCommand cmd7 = new SqlCommand(query7, conexion5);
                                        using (SqlDataReader reader7 = cmd7.ExecuteReader())
                                        {
                                            do
                                            {
                                                while (reader7.Read())
                                                {
                                                    queryimportacionatributos += "INSERT INTO ADICIONALESMODULOSCOMBOS(IDIN,IDADICIONALMODULO,VALOR,ACTIVO) " +
                                                        "VALUES((SELECT MAX(IDIN) FROM ADICIONALESMODULOSCOMBOS) + 1,@CACHEIDATRIBUTO,'" + reader7[2] + "',1); ";
                                                }
                                            } while (reader7.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                                            reader7.Close();
                                        }

                                        conexion5.Close();

                                    }
                                    catch (SqlException ex)
                                    {
                                        return JsonConvert.SerializeObject(ex);
                                    }
                                    catch (Exception ex)
                                    {
                                        return JsonConvert.SerializeObject(ex);
                                    }

                                }
                                queryimportacionatributos += "SET IDENTITY_INSERT ADICIONALESMODULOS OFF;SET IDENTITY_INSERT ADICIONALESMODULOSCOMBOS OFF;SET IDENTITY_INSERT ADICIONALESMODULOSVALORES ON;";
                                queryimportacionatributos += "INSERT INTO ADICIONALESMODULOSVALORES(IDIN,IDADICIONALMODULO,IDTABLA,DESCRIPCION,FECHACREADO) " +
                                    "VALUES((SELECT MAX(IDIN) FROM ADICIONALESMODULOSVALORES) + 1,@CACHEIDATRIBUTO,@IDPRODUCTOIMPORTADO,'" + reader[2] + "',GETDATE());";
                            }
                        } while (reader.NextResult());
                    }
                }
                catch (SqlException ex)
                {
                    return JsonConvert.SerializeObject(ex);
                }
                catch (Exception ex)
                {
                    return JsonConvert.SerializeObject(ex);
                }
                conexion.Close();




                try
                {
                    conexion.Open();
                    string query = queryimportacionatributos;

                    SqlCommand cmd = new SqlCommand(query, conexion);
                    //using (SqlDataReader reader = cmd.ExecuteReader())
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                }
                catch (SqlException ex)
                {
                    return JsonConvert.SerializeObject(ex);
                }
                catch (Exception ex)
                {
                    return JsonConvert.SerializeObject(ex);
                }

            }


            //return JsonConvert.SerializeObject(values20);
            return productonuevo;

        }






        [HttpPost("productos/filtrar_tabla_productos2")]
        //[Consumes("application/json")]
        public string filtrar_tabla_productos2([FromBody] filtrar_tabla_productos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var atributos = JsonConvert.DeserializeObject<List<atributo_valor>>(pInfo);

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            string query1 = "";
            string querycache = "";
            try
            {
                conexion.Open();


                if (model.CATEGORIA != 0 && model.SUBCATEGORIA == 0 && atributos.Count == 0)
                {
                    query1 = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                        "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                        "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                        "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL " +
                        "FROM PRODUCTOS AS P1 WHERE CREADOPOR = '" + model.USUARIO + "' AND IDTIPOPRODUCTO = " + model.CATEGORIA;

                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "IDPRODUCTO") { productos += reader[i] + ","; }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0) { productos = productos.Substring(0, productos.Length - 1); }

                }
                else if (model.CATEGORIA != 0 && model.SUBCATEGORIA != 0 && atributos.Count == 0)
                {
                    query1 = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                        "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                        "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                        "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL " +
                        "FROM PRODUCTOS AS P1 WHERE CREADOPOR = '" + model.USUARIO + "' AND IDTIPOPRODUCTO = " + model.CATEGORIA + " AND IDSUBCATEGORIA = " + model.SUBCATEGORIA;

                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "IDPRODUCTO") { productos += reader[i] + ","; }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0) { productos = productos.Substring(0, productos.Length - 1); }
                }
                else if (model.CATEGORIA != 0 && model.SUBCATEGORIA != 0 && atributos.Count != 0)
                {

                    querycache = "SELECT T.IDTABLA AS PRODUCTO FROM ADICIONALESMODULOSVALORES AS T WHERE T.IDTABLA IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "' AND (";
                    foreach (var modelo in atributos)
                    {
                        querycache += "(SELECT COUNT(*) FROM ADICIONALESMODULOSVALORES WHERE IDTABLA = T.IDTABLA AND IDADICIONALMODULO = " + modelo.ATRIBUTO + " AND DESCRIPCION = " + modelo.VALOR + ") != 0 AND ";
                    }
                    querycache = querycache.Substring(0, querycache.Length - 5);
                    querycache += ") GROUP BY T.IDTABLA";

                    query1 = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                        "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                        "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                        "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL " +
                        "FROM PRODUCTOS AS P1 WHERE CREADOPOR = '" + model.USUARIO + "' AND IDTIPOPRODUCTO = " + model.CATEGORIA + " AND IDSUBCATEGORIA = " + model.SUBCATEGORIA + " AND (SELECT COUNT(*) AS TOTAL FROM(" + querycache + ") AS TOTALO) != 0";
                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "IDPRODUCTO") { productos += reader[i] + ","; }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0) { productos = productos.Substring(0, productos.Length - 1); }
                }
                else if (model.CATEGORIA == 0 && model.SUBCATEGORIA == 0 && atributos.Count != 0)
                {

                    querycache = "SELECT T.IDTABLA AS PRODUCTO FROM ADICIONALESMODULOSVALORES AS T WHERE T.IDTABLA IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "' AND (";
                    foreach (var modelo in atributos)
                    {
                        querycache += "(SELECT COUNT(*) FROM ADICIONALESMODULOSVALORES WHERE IDTABLA = T.IDTABLA AND IDADICIONALMODULO = " + modelo.ATRIBUTO + " AND DESCRIPCION = " + modelo.VALOR + ") != 0 AND ";
                    }
                    querycache = querycache.Substring(0, querycache.Length - 5);
                    querycache += ") GROUP BY T.IDTABLA";

                    query1 = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                        "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                        "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                        "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL " +
                        "FROM PRODUCTOS AS P1 WHERE CREADOPOR = '" + model.USUARIO + "' AND (SELECT COUNT(*) AS TOTAL FROM(" + querycache + ") AS TOTALO) != 0";
                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "IDPRODUCTO") { productos += reader[i] + ","; }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0) { productos = productos.Substring(0, productos.Length - 1); }
                }
                else if (model.CATEGORIA != 0 && model.SUBCATEGORIA == 0 && atributos.Count != 0)
                {
                    querycache = "SELECT T.IDTABLA AS PRODUCTO FROM ADICIONALESMODULOSVALORES AS T WHERE T.IDTABLA IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "' AND (";
                    foreach (var modelo in atributos)
                    {
                        querycache += "(SELECT COUNT(*) FROM ADICIONALESMODULOSVALORES WHERE IDTABLA = T.IDTABLA AND IDADICIONALMODULO = " + modelo.ATRIBUTO + " AND DESCRIPCION = " + modelo.VALOR + ") != 0 AND ";
                    }
                    querycache = querycache.Substring(0, querycache.Length - 5);
                    querycache += ") GROUP BY T.IDTABLA";

                    query1 = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO,ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                        "ISNULL(IDSUBCATEGORIA, 0) AS IDSUBCATEGORIA, ISNULL(CODIGO, '' ) AS CODIGO, ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                        "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA, ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                        "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO, ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL " +
                        "FROM PRODUCTOS AS P1 WHERE CREADOPOR = '" + model.USUARIO + "' AND IDTIPOPRODUCTO = " + model.CATEGORIA + " AND (SELECT COUNT(*) AS TOTAL FROM(" + querycache + ") AS TOTALO) != 0";
                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "IDPRODUCTO") { productos += reader[i] + ","; }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0) { productos = productos.Substring(0, productos.Length - 1); }
                }
                else if (model.CATEGORIA == 0 && model.SUBCATEGORIA == 0 && atributos.Count == 0)
                {

                }


                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }






        [HttpPost("productos/filtrar_tabla_productos")]
        //[Consumes("application/json")]
        public string filtrar_tabla_productos([FromBody] filtrar_tabla_productos model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string productos = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var atributos = JsonConvert.DeserializeObject<List<atributo_valor>>(pInfo);


            var pInfo1 = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo1);

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();

            string query1 = "";
            try
            {
                conexion.Open();


                if (model.CATEGORIA != 0 && model.SUBCATEGORIA != 0)
                {
                    query1 = "SELECT IDPRODUCTO FROM PRODUCTOS WHERE IDTIPOPRODUCTO = " + model.CATEGORIA + " AND IDSUBCATEGORIA = " + model.SUBCATEGORIA + " AND CREADOPOR = '" + model.USUARIO + "'";
                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "IDPRODUCTO")
                                    {
                                        productos += reader[i] + ",";
                                    }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0)
                    {
                        productos = productos.Substring(0, productos.Length - 1);
                    }
                }
                else if (model.CATEGORIA != 0)
                {
                    query1 = "SELECT IDPRODUCTO FROM PRODUCTOS WHERE IDTIPOPRODUCTO = " + model.CATEGORIA + " AND CREADOPOR = '" + model.USUARIO + "'";
                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "IDPRODUCTO")
                                    {
                                        productos += reader[i] + ",";
                                    }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0)
                    {
                        productos = productos.Substring(0, productos.Length - 1);
                    }
                }

                if (productos.Length != 0 && atributos.Count != 0)
                {
                    query1 = "SELECT T.IDTABLA AS PRODUCTO FROM ADICIONALESMODULOSVALORES AS T WHERE T.IDTABLA IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "' AND (";
                    foreach (var modelo in atributos)
                    {
                        query1 += "(SELECT COUNT(*) FROM ADICIONALESMODULOSVALORES WHERE IDTABLA = T.IDTABLA AND IDADICIONALMODULO = " + modelo.ATRIBUTO + " AND DESCRIPCION = " + modelo.VALOR + ") != 0 AND ";
                    }
                    query1 = query1.Substring(0, query1.Length - 5);
                    query1 += ") GROUP BY T.IDTABLA";

                    productos = "";
                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "PRODUCTO")
                                    {
                                        productos += reader[i] + ",";
                                    }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0)
                    {
                        productos = productos.Substring(0, productos.Length - 1);
                    }

                }
                else if (productos.Length == 0 && atributos.Count != 0)
                {
                    query1 = "SELECT T.IDTABLA AS PRODUCTO FROM ADICIONALESMODULOSVALORES AS T WHERE CREADOPOR = '" + model.USUARIO + "' AND (";
                    foreach (var modelo in atributos)
                    {
                        query1 += "(SELECT COUNT(*) FROM ADICIONALESMODULOSVALORES WHERE IDTABLA = T.IDTABLA AND IDADICIONALMODULO = " + modelo.ATRIBUTO + " AND DESCRIPCION = " + modelo.VALOR + ") != 0 AND ";
                    }
                    query1 = query1.Substring(0, query1.Length - 5);
                    query1 += ") GROUP BY T.IDTABLA";
                    productos = "";
                    SqlCommand cmd1 = new SqlCommand(query1, conexion);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetName(i) == "PRODUCTO")
                                    {
                                        productos += reader[i] + ",";
                                    }
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                            }
                        } while (reader.NextResult());
                    }
                    if (productos.Length != 0)
                    {
                        productos = productos.Substring(0, productos.Length - 1);
                    }
                }



                if (productos.Length != 0)
                {
                    string query2 = "SELECT ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO," +
                    "ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0 ) AS IDSUBCATEGORIA," +
                    "ISNULL(CODIGO, '' ) AS CODIGO," +
                    "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE CREADOPOR = '" + model.USUARIO + "' AND IDPRODUCTO IN(" + productos + ") ORDER BY IDPRODUCTO DESC";
                    SqlCommand cmd2 = new SqlCommand(query2, conexion);
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }


                string listas = "";


                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            /*valuesgeneral["Listas de precios"].Add(fieldValues);*/
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }




                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                string querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }



                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }










        [HttpPost("productos/consulta_general")]
        //[Consumes("application/json")]
        public string consulta_general([FromBody] consulta_general model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            /*List<Dictionary<string, object>>()*/
            SqlConnection conexion = new SqlConnection(connString);
            string listas = "";
            string productos = "";
            string atributosseleccion = "";

            var pInfo = JsonConvert.SerializeObject(model.LISTASUCURSALES);
            var sucursales = JsonConvert.DeserializeObject<List<int>>(pInfo);


            string querystock = "";

            var valuesgeneral = new Dictionary<string, List<Dictionary<string, object>>>();
            valuesgeneral["Listas de precios"] = new List<Dictionary<string, object>>();
            valuesgeneral["Productos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Markups particulares"] = new List<Dictionary<string, object>>();
            valuesgeneral["Atributos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Categorias"] = new List<Dictionary<string, object>>();
            valuesgeneral["Combos de atributos"] = new List<Dictionary<string, object>>();
            valuesgeneral["Stock de productos"] = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();




                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY NOMBRE;";
                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDLISTAPRECIO")
                                {
                                    listas += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Listas de precios"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (listas.Length != 0)
                {
                    listas = listas.Substring(0, listas.Length - 1);
                }

                query = "SELECT TOP 25 ISNULL(IDPRODUCTO, 0 ) AS IDPRODUCTO," +
                    "ISNULL(IDTIPOPRODUCTO, 0 ) AS IDTIPOPRODUCTO," +
                    "ISNULL(IDSUBCATEGORIA, 0 ) AS IDSUBCATEGORIA," +
                    "ISNULL(CODIGO, '' ) AS CODIGO," +
                    "ISNULL(DESCRIPCION, '' ) AS DESCRIPCION," +
                    "ISNULL(PORCENTAJEIVA, 0 ) AS PORCENTAJEIVA," +
                    "ISNULL(ACTIVO, 0 ) AS ACTIVO," +
                    "ISNULL(PRECIOCOSTONETO, 0 ) AS PRECIOCOSTONETO," +
                    "ISNULL(PRECIOCOSTOFINAL, 0 ) AS PRECIOCOSTOFINAL FROM PRODUCTOS WHERE CREADOPOR = '" + model.USUARIO + "' ORDER BY IDPRODUCTO DESC";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "IDPRODUCTO")
                                {
                                    productos += reader[i] + ",";
                                }
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Productos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (productos.Length != 0)
                {
                    productos = productos.Substring(0, productos.Length - 1);
                }

                query = "SELECT ISNULL(IDADICIONALMODULO,0) AS IDADICIONALMODULO, ISNULL(ESTILO,'') AS ESTILO, ISNULL(DESCRIPCION,'') AS DESCRIPCION, ISNULL(IDENTIFICADOR,'') AS IDENTIFICADOR FROM ADICIONALESMODULOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY DESCRIPCION";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "ESTILO" && reader[i].ToString() == "SELECCION")
                                {
                                    atributosseleccion += reader[0] + ",";
                                }

                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Atributos"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }
                if (atributosseleccion.Length != 0)
                {
                    atributosseleccion = atributosseleccion.Substring(0, atributosseleccion.Length - 1);
                }

                query = "SELECT ISNULL(IDTIPOPRODUCTO,0) AS IDTIPOPRODUCTO,ISNULL(NOMBRE,'') AS NOMBRE FROM TIPOPRODUCTOS WHERE IDPADRE IS NULL AND ACTIVO = 1 AND CREADOPOR = '" + model.USUARIO + "' ORDER BY NOMBRE";
                cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fieldValues = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }
                            valuesgeneral["Categorias"].Add(fieldValues);
                        }
                    } while (reader.NextResult());
                }


                if (listas.Length != 0 && productos.Length != 0)
                {
                    query = "SELECT ISNULL(IDLISTAPRECIO,0) AS IDLISTAPRECIO,ISNULL(IDPRODUCTO, 0) AS IDPRODUCTO,ISNULL(MARKUP, 0) AS MARKUP," +
                        "ISNULL(PRECIOVENTANETO, 0) AS PRECIOVENTANETO,ISNULL(PRECIOVENTAFINAL, 0) AS PRECIOVENTAFINAL " +
                        "FROM LISTASPRECIOSDETALLES WHERE " +
                        "IDLISTAPRECIO IN(" + listas + ") AND " +
                        "IDPRODUCTO IN(" + productos + ") AND CREADOPOR = '" + model.USUARIO + "'";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Markups particulares"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }
                if (atributosseleccion.Length != 0)
                {
                    query = "SELECT ISNULL(IDADICIONALMODULO,0) AS IDADICIONALMODULO, ISNULL(IDIN,0) AS IDIN, ISNULL(VALOR,'') AS VALOR FROM ADICIONALESMODULOSCOMBOS WHERE IDADICIONALMODULO IN(" + atributosseleccion + ") AND ACTIVO = 1 ORDER BY VALOR";
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Combos de atributos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                querystock = "SELECT IDPRODUCTO,";
                querystock += " (SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + model.SUCURSALES + ") AND IDPRODUCTO = A.IDPRODUCTO) AS SUMATOTAL,";
                foreach (var value in sucursales)
                {
                    querystock += "(SELECT ISNULL(SUM(CANTIDADACTUAL),0) FROM STOCK WHERE IDSUCURSAL IN(" + value + ")  AND IDPRODUCTO = A.IDPRODUCTO) AS SUMA" + value + ",";
                }

                if (sucursales.Count != 0)
                {
                    querystock = querystock.Substring(0, querystock.Length - 1);
                }

                querystock += " FROM STOCK AS A WHERE IDPRODUCTO IN(" + productos + ") GROUP BY IDPRODUCTO";


                if (productos.Length != 0 && sucursales.Count != 0)
                {
                    query = querystock;
                    cmd = new SqlCommand(query, conexion);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var fieldValues = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    fieldValues.Add(reader.GetName(i), reader[i]);
                                }
                                valuesgeneral["Stock de productos"].Add(fieldValues);
                            }
                        } while (reader.NextResult());
                    }
                }




                return JsonConvert.SerializeObject(valuesgeneral);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }








        [HttpPost("listas_de_precios/exportar_listas_de_usuario")]
        //[Consumes("application/json")]
        public string exportar_listas_de_usuario([FromBody] exportar_listas_de_usuario model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT NOMBRE,MARKUP FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND IDLISTAPRECIO IN(" + model.LISTAS + ") AND ACTIVO = 1 ORDER BY IDLISTAPRECIO;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("listas_de_precios/exportar_todas_listas_de_usuario")]
        //[Consumes("application/json")]
        public string exportar_todas_listas_de_usuario([FromBody] exportar_todas_listas_de_usuario model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT NOMBRE,IDLISTAPRECIO FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND ACTIVO = 1 ORDER BY IDLISTAPRECIO;";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }







        [HttpPost("productos/obtener_costos")]
        //[Consumes("application/json")]
        public string obtener_costos_finales([FromBody] obtener_costos_finales model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT ISNULL(IDPRODUCTO,0) AS IDPRODUCTO, ISNULL(PRECIOCOSTOFINAL,0) AS COSTOFINAL, ISNULL(PRECIOCOSTONETO,0) AS COSTONETO FROM PRODUCTOS WHERE CREADOPOR = '" + model.USUARIO + "'";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }








        private static List<T> ConvertToDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }



        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (System.Reflection.PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }



        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        [HttpPost("productos/importar_actualizar")]
        [Consumes("application/json")]
        public string lista_modelos([FromBody] lista_modelos model)
        {

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var modelos = JsonConvert.DeserializeObject<List<importar_actualizar_productos>>(pInfo);

            var pInfo2 = JsonConvert.SerializeObject(model.ENLACES);
            var campos = JsonConvert.DeserializeObject<List<string>>(pInfo2);
            string cadenaenlaces = "";
            foreach (var campo in campos)
            {
                cadenaenlaces += "PRODUCTOS." + campo + " = PRODUCTOSTEMPORALES." + campo + ",";
            }
            cadenaenlaces = cadenaenlaces.Remove(cadenaenlaces.Length - 1, 1);
            string connString = ConexionString;
            DataTable dt = ToDataTable(modelos);
            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand command = new SqlCommand("", conn))
                {
                    try
                    {
                        conn.Open();

                        //Creating temp table on database
                        command.CommandText = "CREATE TABLE PRODUCTOSTEMPORALES(IDPRODUCTO numeric(9),IDTIPOPRODUCTO numeric(9)," +
                        "IDSUBCATEGORIA numeric(9),CODIGO varchar(50),ALIAS varchar(500),DESCRIPCION varchar(500),VENTAPORSESIONES bit," +
                        "MANEJASTOCK bit,DESTACADOTIENDA bit,STOCKMINIMO numeric(9),CODIGOBARRAS varchar(50),CODIGOQR varchar(50)," +
                        "PORCENTAJEIVA numeric(9),PRECIOCOSTONETO decimal(8),PRECIOCOSTOFINAL decimal(8),DURACIONTURNO decimal(9))";
                        command.ExecuteNonQuery();

                        //Bulk insert into temp table
                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                        {
                            bulkcopy.BulkCopyTimeout = 6600;
                            bulkcopy.DestinationTableName = "PRODUCTOSTEMPORALES";
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }

                        // Updating destination table, and dropping temp table
                        command.CommandTimeout = 3000;
                        command.CommandText = "UPDATE PRODUCTOS SET " + cadenaenlaces + " FROM PRODUCTOSTEMPORALES WHERE PRODUCTOS.IDPRODUCTO = PRODUCTOSTEMPORALES.IDPRODUCTO AND PRODUCTOS.CREADOPOR='" + model.USUARIO + "'; DROP TABLE PRODUCTOSTEMPORALES;";
                        command.ExecuteNonQuery();
                        return "Exito";
                    }
                    catch (Exception ex)
                    {
                        return JsonConvert.SerializeObject(ex);
                        // Handle exception properly
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }


        }









        [HttpPost("listas_precios/redondear_precio_neto")]
        [Consumes("application/json")]
        public string redondear_precio_neto([FromBody] lista_redondear_markups model)
        {

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var modelos = JsonConvert.DeserializeObject<List<redondear_markups>>(pInfo);

            string connString = ConexionString;
            DataTable dt = ToDataTable(modelos);
            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand command = new SqlCommand("", conn))
                {
                    try
                    {
                        conn.Open();

                        //Creating temp table on database
                        command.CommandText = "CREATE TABLE MARKUPSTEMPORALES(LISTAPRECIO numeric(9),PRODUCTO numeric(9),MARKUP decimal(9))";
                        command.ExecuteNonQuery();

                        //Bulk insert into temp table
                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                        {
                            bulkcopy.BulkCopyTimeout = 6600;
                            bulkcopy.DestinationTableName = "MARKUPSTEMPORALES";
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }

                        // Updating destination table, and dropping temp table
                        command.CommandTimeout = 3000;
                        command.CommandText = "UPDATE LISTASPRECIOSDETALLES SET MARKUP = MARKUPSTEMPORALES.MARKUP FROM MARKUPSTEMPORALES WHERE " +
                            "LISTASPRECIOSDETALLES.IDPRODUCTO = MARKUPSTEMPORALES.PRODUCTO AND LISTASPRECIOSDETALLES.IDLISTAPRECIO = MARKUPSTEMPORALES.LISTAPRECIO AND LISTASPRECIOSDETALLES.CREADOPOR='" + model.USUARIO + "'; DROP TABLE MARKUPSTEMPORALES;";
                        command.ExecuteNonQuery();
                        return "Exito";
                    }
                    catch (Exception ex)
                    {
                        return JsonConvert.SerializeObject(ex);
                        // Handle exception properly
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }


        }










        [HttpPost("productos/importar_actualizar_listas_precios")]
        [Consumes("application/json")]
        public string importar_actualizar_listas_precios([FromBody] importar_actualizar_listas_precios model)
        {

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var modelos = JsonConvert.DeserializeObject<List<actualizar_listas_precios>>(pInfo);


            string query = "";
            foreach (var modelo in modelos)
            {
                query += "EXECUTE GrabarMarkupParticular '" + model.USUARIO + "'," + modelo.LISTA + "," + modelo.PRODUCTO + "," + modelo.MARKUP + ";";
            }
            string connString = ConexionString;
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }


        }




        [HttpPost("productos/importar_insertar")]
        [Consumes("application/json")]
        public string importar_insertar_productos([FromBody] importar_productos model)
        {

            //var pInfo = JsonConvert.SerializeObject(model.LISTA);
            //var modelos = JsonConvert.DeserializeObject<List<importar_insertar_productos>>(pInfo);


            string query = model.LISTA;
            /*foreach (var modelo in modelos)
            {
                query += "EXECUTE ImportarProducto '" + model.USUARIO + "'," + modelo.IDTIPOPRODUCTO + "," + modelo.IDSUBCATEGORIA + ",'" + modelo.CODIGO + "','" + modelo.ALIAS + "','" + modelo.DESCRIPCION + "'," + modelo.VENTAPORSESIONES + "," + modelo.MANEJASTOCK + "," + modelo.DESTACADOTIENDA + "," + modelo.STOCKMINIMO + ",'" + modelo.CODIGOBARRAS + "','" + modelo.CODIGOQR + "'," + modelo.PORCENTAJEIVA + "," + modelo.PRECIOCOSTONETO + "," + modelo.PRECIOCOSTOFINAL + "," + modelo.DURACIONTURNO + ";";
            }*/
            string connString = ConexionString;
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }


        }








        [HttpPost("productos/filtrar_todos_productos_por_atributos")]
        [Consumes("application/json")]
        public string filtrar_todos_productos_por_atributos([FromBody] filtrar_todos_productos_por_atributos model)
        {

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var modelos = JsonConvert.DeserializeObject<List<atributo_valor>>(pInfo);


            string query = "SELECT T.IDTABLA AS PRODUCTO FROM ADICIONALESMODULOSVALORES AS T WHERE CREADOPOR = '" + model.USUARIO + "' AND (";
            foreach (var modelo in modelos)
            {
                query += "(SELECT COUNT(*) FROM ADICIONALESMODULOSVALORES WHERE IDTABLA = T.IDTABLA AND IDADICIONALMODULO = " + modelo.ATRIBUTO + " AND DESCRIPCION = " + modelo.VALOR + ") != 0 AND ";
            }
            query = query.Substring(0, query.Length - 5);
            query += ") GROUP BY T.IDTABLA";

            string connString = ConexionString;
            SqlConnection conexion = new SqlConnection(connString);

            var values = new List<Dictionary<string, object>>();
            try
            {

                conexion.Open();

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }


        }







        [HttpPost("productos/filtrar_productos_puntuales_por_atributos")]
        [Consumes("application/json")]
        public string filtrar_productos_puntuales_por_atributos([FromBody] filtrar_productos_puntuales_por_atributos model)
        {

            var pInfo = JsonConvert.SerializeObject(model.LISTA);
            var modelos = JsonConvert.DeserializeObject<List<atributo_valor>>(pInfo);


            string query = "SELECT T.IDTABLA AS PRODUCTO FROM ADICIONALESMODULOSVALORES AS T WHERE IDTABLA IN(" + model.PRODUCTOS + ") AND CREADOPOR = '" + model.USUARIO + "' AND (";
            foreach (var modelo in modelos)
            {
                query += "(SELECT COUNT(*) FROM ADICIONALESMODULOSVALORES WHERE IDTABLA = T.IDTABLA AND IDADICIONALMODULO = " + modelo.ATRIBUTO + " AND DESCRIPCION = " + modelo.VALOR + ") != 0 AND ";
            }
            query = query.Substring(0, query.Length - 5);
            query += ") GROUP BY T.IDTABLA";

            string connString = ConexionString;
            SqlConnection conexion = new SqlConnection(connString);

            var values = new List<Dictionary<string, object>>();
            try
            {

                conexion.Open();

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }


        }







        [HttpPost("productos/importar_productos")]
        //[Consumes("application/json")]
        public string importar_productos([FromBody] List<Producto> model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            string resultado;
            try
            {

                foreach (Producto producto in model)
                {
                    Console.WriteLine(producto);
                    resultado = producto.DESCRIPCION;
                }

                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }




        [HttpPost("listas_de_precios/editar_lista_precios")]
        //[Consumes("application/json")]
        public string editar_lista_precios([FromBody] editar_lista_precios model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE LISTASPRECIOS SET " + model.CAMPO + " = " + model.VALOR + " WHERE CREADOPOR = '" + model.USUARIO + "' AND IDLISTAPRECIO = " + model.ID_LISTA + " AND ACTIVO = 1";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("listas_de_precios/eliminar_lista_precios")]
        //[Consumes("application/json")]
        public string eliminar_lista_precios([FromBody] eliminar_lista_precios model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE LISTASPRECIOS SET ACTIVO = 0 WHERE CREADOPOR = '" + model.USUARIO + "' AND IDLISTAPRECIO IN(" + model.ID_LISTA + ") AND ACTIVO = 1";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }





        [HttpPost("listas_de_precios/consultar_lista_precios_particular")]
        //[Consumes("application/json")]
        public string consultar_lista_precios_particular([FromBody] consultar_lista_precios_particular model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            var values = new List<Dictionary<string, object>>();
            try
            {
                conexion.Open();
                string query = "SELECT * FROM LISTASPRECIOS WHERE CREADOPOR = '" + model.USUARIO + "' AND IDLISTAPRECIO = " + model.ID_LISTA + " AND ACTIVO = 1";

                SqlCommand cmd = new SqlCommand(query, conexion);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    do
                    {
                        while (reader.Read())
                        {
                            // define the dictionary
                            var fieldValues = new Dictionary<string, object>();

                            // fill up each column and values on the dictionary                 
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                fieldValues.Add(reader.GetName(i), reader[i]);
                            }

                            // add the dictionary on the values list
                            values.Add(fieldValues);

                        }
                    } while (reader.NextResult());   // if you have multiple result sets on the Stored Procedure, use this. Otherwise you could remove the do/while loop and use just the while.                  
                }
                return JsonConvert.SerializeObject(values);
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("atributos/editar_combo_atributo_seleccion")]
        //[Consumes("application/json")]
        public string editar_combo_atributo_seleccion([FromBody] editar_combo_atributo_seleccion model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE ADICIONALESMODULOSCOMBOS SET VALOR = '" + model.VALOR + "' WHERE " +
                    "IDIN = " + model.ID_COMBO + " AND " +
                    "IDADICIONALMODULO = " + model.ID_ATRIBUTO + " AND " +
                    "ACTIVO = 1 AND " +
                    "(SELECT COUNT(*) FROM ADICIONALESMODULOS WHERE CREADOPOR = '" + model.USUARIO + "' AND IDADICIONALMODULO = " + model.ID_ATRIBUTO + ") >= 1";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }



        [HttpPost("atributos/eliminar_combo_atributo_seleccion")]
        //[Consumes("application/json")]
        public string eliminar_combo_atributo_seleccion([FromBody] eliminar_combo_atributo_seleccion model)
        {
            string connString = ConexionString;
            /*Console.WriteLine(ConfigurationManager.ConnectionStrings[0].ToString());*/
            SqlConnection conexion = new SqlConnection(connString);
            try
            {
                conexion.Open();
                string query = "UPDATE ADICIONALESMODULOSCOMBOS SET ACTIVO = 0 WHERE " +
                    "IDIN IN(" + model.COMBOS + ") AND " +
                    "(SELECT COUNT(*) FROM ADICIONALESMODULOS WHERE CREADOPOR = '" + model.USUARIO + "' AND IDADICIONALMODULO = " + model.ID_ATRIBUTO + ") = 1";

                SqlCommand cmd = new SqlCommand(query, conexion);
                //using (SqlDataReader reader = cmd.ExecuteReader())
                cmd.ExecuteNonQuery();
                conexion.Close();
                return "Exito";
            }
            catch (SqlException ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }








        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get2(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public string Post([FromBody]string value)
        {
            return value;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
