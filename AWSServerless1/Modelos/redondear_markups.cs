﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class redondear_markups
    {
        public int LISTAPRECIO { get; set; }
        public int PRODUCTO { get; set; }
        public decimal MARKUP { get; set; }
    }
}
