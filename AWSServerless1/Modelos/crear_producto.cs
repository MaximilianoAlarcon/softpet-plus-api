﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class crear_producto
    {
        public int CATEGORIA { get; set; }
        public int SUBCATEGORIA { get; set; }
        public string CODIGO { get; set; }
        public string DESCRIPCION { get; set; }
        public string ALIAS { get; set; }
        public string CODIGOBARRAS { get; set; }
        public int MANEJASTOCK { get; set; }
        public int STOCKMINIMO { get; set; }
        public decimal PORCENTAJEIVA { get; set; }
        public int PERMITEAGENDAR { get; set; }
        public decimal DURACIONTURNO { get; set; }
        public int DESTACADO { get; set; }
        public string CREADOPOR { get; set; }
        public string CODIGOQR { get; set; }
    }
}
