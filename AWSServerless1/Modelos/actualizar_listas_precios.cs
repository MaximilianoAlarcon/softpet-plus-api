﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class actualizar_listas_precios
    {
        public int LISTA { get; set; }
        public int PRODUCTO { get; set; }
        public decimal MARKUP { get; set; }
    }
}
