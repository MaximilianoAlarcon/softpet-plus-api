﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class crear_atributo_texto
    {
        public string IDENTIFICADOR { get; set; }
        public string DESCRIPCION { get; set; }
        public string ESTILO { get; set; }
        public string ID_USUARIO { get; set; }
    }
}
