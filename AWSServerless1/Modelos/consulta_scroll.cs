﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class consulta_scroll
    {
        public string USUARIO { get; set; }
        public int PRODUCTO { get; set; }
        public string SUCURSALES { get; set; }
        public List<int> LISTASUCURSALES { get; set; } = new List<int>();
    }
}
