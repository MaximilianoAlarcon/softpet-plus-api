﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class Producto
    {
        public int IDPRODUCTO { get; set; }
        public int IDTIPOPRODUCTO { get; set; }
        public int IDSUBCATEGORIA { get; set; }
        public string CODIGO { get; set; }
        public string DESCRIPCION { get; set; }
        public string ALIAS { get; set; }
        public string CODIGOBARRAS { get; set; }
        public int MANEJASTOCK { get; set; }
        public decimal STOCKMINIMO { get; set; }
        public decimal PORCENTAJEIVA { get; set; }
        public int VENTAPORSESIONES { get; set; }
        public int DURACIONTURNO { get; set; }
        public int MODIFICADURACION { get; set; }
        public int DESTACADOTIENDA { get; set; }
        public int ACTIVO { get; set; }
        public string CREADOPOR { get; set; }
        public decimal PRECIOCOSTONETO { get; set; }
        public decimal PRECIOCOSTOFINAL { get; set; }
        public string CODIGOQR { get; set; }
    }
}
