﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class grabar_markup_particular_de_producto
    {
        public int IDPRODUCTO { get; set; }
        public int IDLISTAPRECIO { get; set; }
        public decimal MARKUP { get; set; }
        public decimal NETO { get; set; }
        public decimal FINAL { get; set; }
        public string USUARIO { get; set; }
    }
}
