﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class editar_combo_atributo_seleccion
    {
        public string USUARIO { get; set; }
        public string VALOR { get; set; }
        public int ID_ATRIBUTO { get; set; }
        public int ID_COMBO { get; set; }
    }
}
