﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class consultar_buscador
    {
        public string USUARIO { get; set; }
        public string SUCURSALES { get; set; }
        public List<int> LISTASUCURSALES { get; set; } = new List<int>();
        public string BUSQUEDA { get; set; }
    }
}
