﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class modificar_markups_de_listas
    {
        public string ARRAYPRODS { get; set; }
        public string DEFAULTMARKUPS { get; set; }
        public string ARRAYLISTASPRECIOS { get; set; }

        public decimal MARKUP { get; set; }
    }
}
