﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class lista_modelos
    {
        public List<importar_actualizar_productos> LISTA { get; set; } = new List<importar_actualizar_productos>();
        public string USUARIO { get; set; }
        public List<string> ENLACES { get; set; } = new List<string>();
    }
}
