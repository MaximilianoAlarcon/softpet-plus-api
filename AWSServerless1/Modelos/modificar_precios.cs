﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class modificar_precios
    {
        public int LISTAPRECIO { get; set; }
        public int PRODUCTO { get; set; }
        public decimal MARKUP { get; set; }
        public decimal NETO { get; set; }
        public decimal FINAL { get; set; }
    }
}
