﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class editar_lista_precios
    {
        public string CAMPO { get; set; }
        public string VALOR { get; set; }
        public string USUARIO { get; set; }
        public int ID_LISTA { get; set; }
    }
}
