﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class filtrar_tabla_productos
    {
        public List<atributo_valor> LISTA { get; set; } = new List<atributo_valor>();
        public string USUARIO { get; set; }
        public int CATEGORIA { get; set; }
        public int SUBCATEGORIA { get; set; }
        public string SUCURSALES { get; set; }
        public List<int> LISTASUCURSALES { get; set; } = new List<int>();
    }
}
