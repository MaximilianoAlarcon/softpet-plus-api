﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSServerless1.Modelos
{
    public class grabar_valores_correlativos
    {
        public int IDPRODUCTO { get; set; }
        public decimal IVA { get; set; }
        public decimal PRECIOCOSTONETO { get; set; }
        public decimal PRECIOCOSTOFINAL { get; set; }
    }
}
